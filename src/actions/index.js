/* @flow */
import type {ShowProps} from "../containers/ShowsContainer";

export const REQUEST_SHOWS = "REQUEST_SHOWS";
export const RECEIVE_SHOWS = "RECEIVE_SHOWS";

export const requestShows = () => ({
  type: REQUEST_SHOWS
});

export const receiveShows = (shows: ShowProps[]) => ({
  type: RECEIVE_SHOWS,
  shows
});

export const fetchShows = () => (dispatch: *) => {
  dispatch(requestShows());
  // this was the request I made with the local server, but to work in the sandbox
  // I added the json directly
  // return fetch("http://localhost:3001/shows")
  //   .then(response => response.json())
  //   .then(shows => dispatch(receiveShows(shows)));
  return import("../db.json").then((data: *) => {
    dispatch(receiveShows(data.shows));
  });
};
