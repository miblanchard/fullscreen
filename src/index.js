import React from "react";
import {render} from "react-dom";
import {createStore, applyMiddleware} from "redux";
import {Provider} from "react-redux";
import thunk from "redux-thunk";
import {showsState} from "./reducers";
import {ShowsContainer} from "./containers/ShowsContainer.js";

const store = createStore(showsState, applyMiddleware(thunk));

const styles = {
  backgroundColor: "#000000",
  color: "#757575",
  display: "flex",
  fontFamily: "sans-serif",
  flexDirection: "column",
  textAlign: "center",
  padding: 16
};

export const App = () => (
  <div style={styles}>
    <ShowsContainer />
  </div>
);

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);
