/* @flow */
import * as React from "react";
import {connect} from "react-redux";
import {fetchShows} from "../actions";
import {Show} from "../components/Show.js";

export type ShowProps = {|
  name: string,
  seasons: Array<*>,
  summary: string
|};

type BaseShowsContainerProps = {|
  dispatch: *,
  isFetching: boolean,
  shows: Array<ShowProps>
|};

class BaseShowsContainer extends React.PureComponent<BaseShowsContainerProps> {
  componentDidMount() {
    this.props.dispatch(fetchShows());
  }

  render() {
    const {isFetching, shows} = this.props;

    if (isFetching || !shows || shows.length === 0) {
      return <div> Loading Data... </div>;
    }

    return (
      <React.Fragment>
        <h1>Fullscreen Direct Shows</h1>
        {shows.map((show, index) => (
          <Show
            key={show.name}
            name={show.name}
            seasons={show.seasons}
            summary={show.summary}
          />
        ))}
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  const {isFetching, shows} = state || {
    isFetching: true,
    shows: []
  };

  return {
    shows,
    isFetching
  };
};

export const ShowsContainer = connect(mapStateToProps)(BaseShowsContainer);
