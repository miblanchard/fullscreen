import {REQUEST_SHOWS, RECEIVE_SHOWS} from "../actions";

const shows = (
  state = {
    isFetching: false,
    shows: []
  },
  action
) => {
  switch (action.type) {
    case REQUEST_SHOWS:
      return {
        ...state,
        isFetching: true
      };
    case RECEIVE_SHOWS:
      return {
        ...state,
        isFetching: false,
        shows: action.shows
      };
    default:
      return state;
  }
};

export const showsState = (state = {}, action) => {
  switch (action.type) {
    case RECEIVE_SHOWS:
    case REQUEST_SHOWS:
      return {
        ...state,
        ...shows(state, action)
      };
    default:
      return state;
  }
};
