/* @flow */
import * as React from "react";
import {SeasonList} from "./SeasonList.js";
import Parser from "html-react-parser";

import type {Season} from "./SeasonList";

const styles = {
  summary: {
    fontSize: 12
  }
};

export type ShowProps = {|
  name: string,
  seasons: Season[],
  summary: string
|};

export const Show = ({name, seasons, summary}: ShowProps) => (
  <React.Fragment>
    <h2>{name}</h2>
    <div style={styles.summary}>{Parser(summary)}</div>
    {seasons.map((season, index) => (
      <SeasonList
        key={`${name} season ${index + 1}`}
        seasonData={season}
        seasonNumber={index + 1}
      />
    ))}
  </React.Fragment>
);
