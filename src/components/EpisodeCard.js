/* @flow */
import * as React from "react";
import {Card, Image} from "semantic-ui-react";
import Parser from "html-react-parser";

const styles = {
  content: {
    backgroundColor: "#FFFFFF",
    height: "100%"
  },
  image: {
    width: "100%"
  },
  wrapper: {
    border: "1px solid black",
    display: "flex",
    flexDirection: "column",
    margin: 8,
    width: "30%"
  }
};

export type EpisodeProps = {|
  caption: string,
  description: string,
  picture_url: string,
  title: string
|};

export const EpisodeCard = ({
  caption,
  description,
  picture_url,
  title
}: EpisodeProps) => {
  return (
    <Card style={styles.wrapper}>
      <Image
        src={
          picture_url ||
          "https://pmcvariety.files.wordpress.com/2014/11/rooster-teeth-logo.jpg?w=480&h=270&crop=1"
        }
        style={styles.image}
      />
      <Card.Content style={styles.content}>
        <Card.Header>{title}</Card.Header>
        <Card.Meta>{caption}</Card.Meta>
        <Card.Description>{Parser(description)}</Card.Description>
      </Card.Content>
    </Card>
  );
};
