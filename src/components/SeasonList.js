/* @flow */
import * as React from "react";
import {Button} from "semantic-ui-react";
import {EpisodeCard} from "./EpisodeCard.js";

import type {EpisodeProps} from "./EpisodeCard";

const styles = {
  episodeList: {
    alignItems: "stretch",
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "center"
  }
};

export type Season = {|
  slug: string,
  title: string,
  show_episodes: EpisodeProps[]
|};

type SeasonListProps = {|
  seasonData: Season,
  seasonNumber: number
|};

type SeasonListState = {
  episodes: EpisodeProps[],
  filteredEpisodes: ?(EpisodeProps[]),
  sortDirection: number
};

export class SeasonList extends React.PureComponent<
  SeasonListProps,
  SeasonListState
> {
  state = {
    episodes: this.props.seasonData.show_episodes,
    filteredEpisodes: null,
    sortDirection: 1
  };

  componentWillReceiveProps(nextProps: SeasonListProps) {
    this.setState(() => ({episodes: nextProps.seasonData.show_episodes}));
  }

  filterEpisodes = (event: SyntheticInputEvent<HTMLInputElement>) => {
    const input = event.target.value;
    this.setState(prevState => {
      const {episodes} = prevState;
      const filteredEpisodes = episodes.filter(episode => {
        return (
          episode.description.toLowerCase().search(input.toLowerCase()) !== -1
        );
      });
      return input ? {filteredEpisodes} : {filteredEpisodes: null};
    });
  };

  sortEpisodesBy = (field: string) => {
    this.setState(prevState => {
      const episodes = prevState.episodes.sort((a, b) => {
        if (a[field] > b[field]) {
          return -prevState.sortDirection;
        }
        if (a[field] < b[field]) {
          return prevState.sortDirection;
        }
        return 0;
      });
      return {
        episodes,
        sortDirection: -prevState.sortDirection
      };
    });
  };

  render() {
    const {seasonNumber} = this.props;
    const {episodes, filteredEpisodes} = this.state;
    const episodesToShow = !!filteredEpisodes ? filteredEpisodes : episodes;

    return (
      <React.Fragment>
        <h3>{`Season #${seasonNumber}`}</h3>
        <div className="sort-section">
          <span>Sort by: </span>
          <Button onClick={() => this.sortEpisodesBy("title")}>Title</Button>
          <Button onClick={() => this.sortEpisodesBy("caption")}>
            Caption
          </Button>
          <Button onClick={() => this.sortEpisodesBy("description")}>
            Description
          </Button>
        </div>
        <input
          type="text"
          placeholder="Search Episode Descriptions"
          onChange={this.filterEpisodes}
        />
        <div style={styles.episodeList}>
          {episodesToShow.map((episode, index) => (
            <EpisodeCard
              caption={episode.caption}
              description={episode.description}
              key={episode.title}
              picture_url={episode.picture_url}
              title={episode.title}
            />
          ))}
        </div>
      </React.Fragment>
    );
  }
}
